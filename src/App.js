import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import {createBrowserHistory} from 'history';
import PopularMovies from './hocs/Movies';
import Movie from './hocs/Movie';

const history = createBrowserHistory();
function App() {
  return (
      <Router history={history} basename={""} >
        <Switch>
          <Route path='/' component={PopularMovies} exact />
          <Route path='/:id' component={Movie}  />
        </Switch>
      </Router>
  );
}

export default App;
