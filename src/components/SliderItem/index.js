import React from 'react';
import { Link } from 'react-router-dom';


const SliderItem  = ({id, path, title}) =>(
    <Link className='sliderItem' to={`/${id}`}>
        <img src={path} alt={title} className="poster"/>
        <span>{title}</span>
    </Link>
);

export default SliderItem;
