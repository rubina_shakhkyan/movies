import React, { Component } from 'react';
import ItemsCarousel from 'react-items-carousel';
import SliderItem from '../SliderItem';
import placeholder from '../../assets/img/poster-placeholder.jpg';
import './styles.css';

const cards = window.innerWidth > 1600 ? 7 : window.innerWidth > 1024 ? 5 : window.innerWidth > 800 ? 4 : window.innerWidth > 600 ? 3 : 2;

export default class MoviesType extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeItemIndex: 0,
        }
    }
    componentDidMount(){
        this.props.loadData(this.props.type);
    }
    changeActiveItem = (activeItemIndex) => {
        if(activeItemIndex === this.props.data.length-cards+Math.floor(cards/2) && this.props.page){
            this.props.loadData(this.props.type, this.props.page);
        }
        this.setState({ activeItemIndex });
    };


    render() {
        const {
            activeItemIndex,
        } = this.state;
        return (
            <div>
                <h3>{this.props.title} </h3>
                {
                    this.props.data instanceof Array ? (
                        <ItemsCarousel
                            numberOfCards={cards}
                            gutter={20}
                            showSlither={true}
                            firstAndLastGutter={true}
                            freeScrolling={false}

                            requestToChangeActive={this.changeActiveItem}
                            activeItemIndex={activeItemIndex}
                            activePosition={'center'}

                            chevronWidth={24}
                            rightChevron={'>'}
                            leftChevron={'<'}
                            outsideChevron={false}
                        >
                            {this.props.data.map( (el) => (<SliderItem key={el.id} id={el.id} path={el.poster_path ? `http://image.tmdb.org/t/p/w342/${el.poster_path}` : placeholder} title={this.props.type==='tv' ? el.name : el.title}/>))}
                        </ItemsCarousel>
                    ): (
                            <p>No data available for this category</p>
                    )
                }
            </div>
        )
    }
}
