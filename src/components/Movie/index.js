import React, { Component } from 'react';
import Player from '../Player';
import './styles.css';
import placeholder from '../../assets/img/poster-placeholder.jpg';

export default class Movie extends Component {
    state = {
        playerOpen: false,
    };

    componentDidMount(){
        this.props.loadDetails(this.props.match.params.id);
    }

    openPlayer = () => {
        this.setState({
           playerOpen: true,
        });
    };

    render() {
        return (
            <div className="container">
                {
                    this.props.data instanceof Object ? (
                            this.state.playerOpen ? (<Player poster={this.props.data.poster_path ? `http://image.tmdb.org/t/p/w342/${this.props.data.poster_path}` : placeholder}/>) : (
                                <div>
                                    <div className="col-6">
                                        <h2>{this.props.data.title}</h2>
                                        <h3>{this.props.data.tagline}</h3>
                                        <p className="text-left">{this.props.data.overview}</p>
                                        <hr/>
                                        <button onClick={this.openPlayer} className="playButton">Play</button>
                                    </div>
                                    <div className="col-6">
                                        <img alt={this.props.data.title} src={this.props.data.poster_path ? `http://image.tmdb.org/t/p/w342/${this.props.data.poster_path}` : placeholder} />
                                    </div>
                                </div>
                            )

                    ):(
                        null
                    )
                }
            </div>
        )
    }
}
