import React, {Component} from 'react';
import MoviesType from '../MoviesType';
import SearchBar from '../SearchBar';

export default class Movies extends Component {
    render() {
        return (
            <div  className='container'>
                <SearchBar search={(query) => this.props.search(query)}/>
                {
                    this.props.searchResults instanceof Object  ? (
                        <MoviesType type='movie' title='Search results' loadData={()=> {}}
                                    data={this.props.searchResults}/>
                    ) : (
                        <div>
                            <MoviesType type='movie' title='Popular Movies' loadData={this.props.loadData}
                                        data={typeof this.props.data !== "undefined" ? this.props.data.movie : []}
                                        page={this.props.page instanceof Object ? this.props.page.movie : 1}
                            />
                            <MoviesType type='tv' title='Popular TV Shows' loadData={this.props.loadData}
                                        data={typeof this.props.data !== "undefined" ? this.props.data.tv : []}
                                        page={this.props.page instanceof Object ? this.props.page.tv: 1}
                            />
                            <MoviesType type='family' title='Family' loadData={this.props.loadData}
                                        data={typeof this.props.data !== "undefined" ? this.props.data.family : []}
                                        page={this.props.page instanceof Object ? this.props.page.family: 1}
                            />
                            <MoviesType type='documentary' title='Documentary' loadData={this.props.loadData}
                                        data={typeof this.props.data !== "undefined" ? this.props.data.documentary : []}
                                        page={this.props.page instanceof Object ? this.props.page.documentary : 1}
                            />
                        </div>
                    )
                }

            </div>
        )
    }
}
