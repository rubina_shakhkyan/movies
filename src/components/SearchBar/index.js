import React, { Component } from 'react';
import './style.css';

export default class SearchBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            searchValue: '',
        }
    }

    setValue = (searchValue) => {
        this.setState({
            searchValue
        });
    }

    render() {
        return (
            <form className="form" onSubmit={(e)=>{e.preventDefault(); this.props.search(this.state.searchValue)}}>
                <input type="text" placeholder="Type here..." name="search" onChange={(e)=>this.setValue(e.target.value)} className="search"/>
                <button type="submit" className="searchButton">Search</button>
            </form>
        )
    }
}
