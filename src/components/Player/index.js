import React, { Component } from 'react';
import shaka from 'shaka-player'
import * as video_uri from '../../assets/video/playlist.m3u8';


export default class Player extends Component {
    componentDidMount() {
        shaka.polyfill.installAll();

        if (shaka.Player.isBrowserSupported()) {
            this.initPlayer();
        } else {
            console.error('Browser not supported!');
        }
    }

    initPlayer(){
        shaka.media.ManifestParser.registerParserByExtension("m3u8", shaka.hls.HlsParser);
        shaka.media.ManifestParser.registerParserByMime("Application/vnd.apple.mpegurl", shaka.hls.HlsParser);
        shaka.media.ManifestParser.registerParserByMime("application/x-mpegURL", shaka.hls.HlsParser);
        let player = new shaka.Player(this.refs.video);

        player.addEventListener('error', this.onErrorEvent);
        player.load(video_uri).then(function() {
            console.log('The video has now been loaded!');
        }).catch(this.onError);
    }

    onErrorEvent(event) {
        this.onError(event.detail);
    }

    onError(error) {
        console.error('Error code', error.code, 'object', error);
    }

    render() {
        return (
                <video
                    ref="video"
                    height="500"
                    width="80%"
                    poster={this.props.poster}
                    controls
                    autoPlay
                >
                </video>
        )
    }
}
