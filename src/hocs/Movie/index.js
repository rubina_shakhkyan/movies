import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MovieComponent from '../../components/Movie';
import { loadDetails } from '../../store/actions';


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loadDetails}, dispatch);
};

const Movie = connect(state => ({
    data: state.movie
}), mapDispatchToProps)(MovieComponent)

export default Movie;