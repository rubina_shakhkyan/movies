import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MoviesComponent from '../../components/Movies';
import { loadData, search } from '../../store/actions';


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({loadData, search}, dispatch);
};

const Movies = connect(state => ({
    data: state.movies,
    searchResults: state.search,
    page: state.page
}), mapDispatchToProps)(MoviesComponent);

export default Movies;
