import * as types from '../constants';

const movies = (state={}, action) => {
    switch (action.type) {
        case types.ERROR:
            return {...state, error: action.error};
        case types.LOAD_MOVIES:
            let movies = state.movies instanceof  Object ? state.movies : {};
            let type = movies.hasOwnProperty(action.movieType) ? movies[action.movieType] : [];
            return {...state, movies: { ...state.movies, [action.movieType]: typeof action.data !== 'undefined' ? type.concat(action.data.results)  : []}, page: {...state.page, [action.movieType] : action.data.page  < action.data.total_pages ? action.data.page + 1 : false}};
        case types.LOAD_DETAILS:
            return {...state, movie:  action.data };
        case types.LOAD_SEARCH:
            return {...state, search:  typeof action.data !== 'undefined' ? action.data.results : [], page: {...state.page, search : action.data.page  < action.data.total_pages ? action.data.page + 1 : false }};
        default:
            return state;
    }
};


export default movies;
