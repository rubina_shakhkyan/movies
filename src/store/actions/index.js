import axios from 'axios';
import * as types from '../constants';

const api_uri = 'https://api.themoviedb.org/3/';
const api_key = 'api_key=3a427587dd737e0083151d88f0d2164f';

const receiveMovies = (data, movieType) => ({
    type: types.LOAD_MOVIES,
    data,
    movieType
});

const receiveDetails = (data) => ({
    type: types.LOAD_DETAILS,
    data,
});

const receiveError = (error) => ({
    type: types.ERROR,
    error
});

const receiveSearch = (data) => ({
    type: types.LOAD_SEARCH,
    data,
});

export const loadData = (type, page = 1) => {
    let genre = type === 'documentary' ? 99 : type === 'family' ? 10751 : null;
    let query = genre ? `discover/movie?${api_key}&with_genres=${genre}` : `${type}/popular?${api_key}`;
        return (dispatch) => {
        return axios.get(`${api_uri}${query}&page=${page}`)
            .then(response => {
                dispatch(receiveMovies(response.data, type))
            })
            .catch(error => {
                receiveError(error);
            });
    }
};

export const loadDetails = (id) => {
    return (dispatch) => {
        return axios.get(`${api_uri}movie/${id}?language=en-US&${api_key}`)
            .then(response => {
                dispatch(receiveDetails(response.data))
            })
            .catch(error => {
                receiveError(error);
            });
    }
};

export const search = (query, page = 1) => {
  return (dispatch) => {
      return axios.get(`${api_uri}search/movie?${api_key}&language=en-US&query=${query}&page=${page}&include_adult=false`)
          .then(response => {
              dispatch(receiveSearch(response.data))
          })
          .catch(error => {
              receiveError(error);
          });
  }
};

