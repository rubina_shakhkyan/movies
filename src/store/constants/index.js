export const LOAD_MOVIES = 'LOAD_MOVIES';
export const LOAD_DETAILS = 'LOAD_DETAILS';
export const ERROR = 'ERROR';
export const LOAD_SEARCH = 'LOAD_SEARCH';